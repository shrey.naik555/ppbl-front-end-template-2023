import Lesson3031 from "@/src/components/course-modules/303/3031";
import Lesson3032 from "@/src/components/course-modules/303/3032";
import Lesson3033 from "@/src/components/course-modules/303/3033";
import Lesson3034 from "@/src/components/course-modules/303/3034";
// import Assignment303Page from "@/src/components/course-modules/303/assignment303";
import SLTs303 from "@/src/components/course-modules/303/slts";
import ModuleLessons from "@/src/components/lms/Lesson/Lesson";
import slt from "@/src/data/slts-english.json";

const Module303Lessons = () => {
    const moduleSelected = slt.modules.find((m) => m.number === 303);

    const status = null;

    const lessons = [
        { key: "slts", component: <SLTs303 /> },
        { key: "3031", component: <Lesson3031 /> },
        { key: "3032", component: <Lesson3032 /> },
        { key: "3033", component: <Lesson3033 /> },
        { key: "3034", component: <Lesson3034 /> },
        // { key: "assignment303", component: <Assignment303Page /> },
    ];

    return (
        <ModuleLessons
            items={moduleSelected?.lessons ?? []}
            modulePath="/modules/303"
            selected={0}
            lessons={lessons}
            status={status}
        />
    );
};

export default Module303Lessons;
