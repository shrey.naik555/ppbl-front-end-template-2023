import TeamList from "@/src/components/team/TeamList";
import { Heading, Box, Link as CLink, Text, Divider } from "@chakra-ui/react";
import Head from "next/head";

export default function AboutPage() {
  return (
    <>
      <Head>
        <title>About PPBL 2023</title>
        <meta name="description" content="About Plutus Project-Based Learning" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Box w={{ base: "95%", md: "85%", lg: "75%" }} mx="auto">
        <Heading py="10" size="2xl">
          About Plutus Project-Based Learning
        </Heading>
        <Text fontSize="2xl">
          Plutus Project-Based Learning is a free course from Gimbalabs. The goal of Plutus Project-Based Learning is to support people to become contributors to real projects in the Cardano development ecosystem. The course is self-paced, and anyone can work through course on their own time. We host <CLink href="live-coding">two Live Coding sessions every week</CLink>, where students gather to ask questions and to learn together.
        </Text>
        <Text fontSize="2xl" py="5">
          The best way to learn about the course is to <CLink href="/get-started">Get Started</CLink>. We invite to jump in, start learning, and participate in our growing community of learners and builders.
        </Text>
        <Divider />
        <TeamList />
      </Box>
    </>
  );
}
