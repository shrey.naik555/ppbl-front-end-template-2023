import Assignment302 from "@/src/components/course-modules/302/assignment302/Docs.mdx";
import CommitLayout from "@/src/components/lms/Lesson/CommitLayout";
import { Grid, GridItem, Text, Box, Heading, useColorModeValue } from "@chakra-ui/react";
import CommitmentTx from "@/src/components/gpte/transactions/CommitmentTx";
import { useContext, useEffect, useState } from "react";
import { PPBLContext } from "@/src/context/PPBLContext";
import Link from "next/link";
import { useWallet } from "@meshsdk/react";
import { checkReferenceDatumForPrerequisite, stringToHex } from "@/src/utils";

const Assignment302Page = () => {
  const ppblContext = useContext(PPBLContext);
  const { connected } = useWallet();
  const [hasPrerequisite, setHasPrerequisite] = useState(false);
  const text_color = useColorModeValue("theme.orange", "theme.yellow");

  useEffect(() => {
    if (ppblContext.contributorReferenceDatum) {
      setHasPrerequisite(
        checkReferenceDatumForPrerequisite(ppblContext.contributorReferenceDatum, [stringToHex("Module201")])
      );
    }
  }, [ppblContext.contributorReferenceDatum]);

  return (
    <CommitLayout moduleNumber={302} slug="assignment302">
      <Grid templateColumns="repeat(5, 1fr)" templateRows="repeat(2, 1fr)" gap={5}>
        <GridItem colSpan={[5, 5, 5, 5, 5, 3]} rowSpan={2}>
          <Assignment302 />
        </GridItem>
        <GridItem colSpan={[5, 5, 5, 5, 5, 2]} border="1px" borderColor={text_color} borderRadius="md" p="3">
          {hasPrerequisite ? (
            <>
              <Box my="5">{ppblContext.treasuryUTxO && <CommitmentTx selectedProject={"Module302"} />}</Box>
            </>
          ) : (
            <>
              <Box my="5">
                <Text fontSize="lg" fontWeight="900" color={text_color} pb="3">
                  When you are ready, make a commitment to Module 302. Be sure to complete all of the steps.
                </Text>
                <Text fontSize="lg" fontWeight="900" color={text_color} pb="3">
                  You must complete the <Link href="/modules/204/commit">Module 204 commitment</Link> before you can
                  commit to Module 302.
                </Text>
                {ppblContext.connectedContribToken && (
                  <Text fontSize="sm" fontWeight="300">
                    {ppblContext.connectedContribToken}
                  </Text>
                )}
                {!connected && (
                  <Text fontSize="sm" fontWeight="300">
                    Connect a wallet to check completion status.
                  </Text>
                )}
              </Box>
            </>
          )}
        </GridItem>
      </Grid>
    </CommitLayout>
  );
};

export default Assignment302Page;
