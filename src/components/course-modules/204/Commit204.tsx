import {
	Container,
	Divider,
	Box,
	Button,
	Spacer,
	Flex,
	Heading,
	Grid,
	GridItem,
	Text,
} from '@chakra-ui/react';
import Link from 'next/link';
import React, { useContext, useState } from 'react';

import CommitLayout from '@/src/components/lms/Lesson/CommitLayout';
import CommitmentTx from '@/src/components/gpte/transactions/CommitmentTx';
import Commit from '@/src/components/course-modules/204/Commit.mdx';
import { PPBLContext } from '@/src/context/PPBLContext';

const Commit204 = () => {
	const ppblContext = useContext(PPBLContext);
	const selectedProject = 'Module204';

	return (
		<CommitLayout moduleNumber={204} slug="commit">
			<Grid
				templateColumns='repeat(5, 1fr)'
				templateRows='repeat(2, 1fr)'
				gap={5}>
				<GridItem colSpan={[5, 5, 5, 5, 5, 3]} rowSpan={2}>
					<Commit />
				</GridItem>
				<GridItem
					colSpan={[5, 5, 5, 5, 5, 2]}
					border='1px'
					borderColor='theme.yellow'
					borderRadius='md'
					p='3'>
					<Box my='5'>
						{ppblContext.treasuryUTxO && (
							<CommitmentTx selectedProject={selectedProject} />
						)}
					</Box>
				</GridItem>
			</Grid>
		</CommitLayout>
	);
};

export default Commit204;
