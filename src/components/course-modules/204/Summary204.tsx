import { Container, Divider, Box, Button, Spacer, Flex, Heading } from "@chakra-ui/react";
import Link from "next/link";
import React from "react";

import Summary from "@/src/components/course-modules/204/Summary.mdx";
import SummaryLayout from "@/src/components/lms/Lesson/SummaryLayout";
import FaucetTestInstance from "./components/FaucetTestInstance";

const Summary204 = () => {
  return (
    <SummaryLayout nextModule="301">
      <Summary />
    </SummaryLayout>
  )
};

export default Summary204;
