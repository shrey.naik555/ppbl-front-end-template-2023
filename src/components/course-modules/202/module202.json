{
  "lessons": [
    {
      "slug": "2021",
      "introduction": [
        "GameChanger Wallet provides a convenient way to mint \"native assets\", also known as \"tokens\", on Cardano.",
        "The purpose of this lesson is to show you a quick way to get started minting Cardano tokens. Then, in Lessons 202.2 and beyond, we will investigate how to write special scripts that can be used to create tokens with unique Policy IDs.",
        "Depending on your goals, you might find that GameChanger is all you need. For example, if you want a quick way to mint some tokens for testing an application, GameChanger can be a great option."
      ],
      "links": [
        {
          "url": "https://gamechanger.finance/",
          "linkText": "GameChanger Homepage"
        },
        {
          "url": "https://preprod-wallet.gamechanger.finance/",
          "linkText": "GameChanger Preprod Wallet"
        }
      ],
      "videoHeading": "Minting Tokens with GameChanger",
      "youtubeId": "wgVhnBoMcIU",
      "successComponent": false,
      "success": {
        "criteria": "",
        "text": ""
      },
      "assignmentComponent": true,
      "markdownComponent": false
    },
    {
      "slug": "2022",
      "introduction": [
        "Every asset on Cardano is identified by a \"Policy ID\" and a \"Token Name\". A Policy ID is derived from a certain set of rules that can be specified in a script.",
        "There are two kinds of scripts on Cardano: \"Native Scripts\" and \"Plutus Scripts\". The difference between the two is introduced in this lesson.",
        "In this lesson, you will see how to use a native script to mint assets on Cardano. Then, in lessons 202.4 and 202.5, you will see how to create Plutus scripts that can be used in a similar way."
      ],
      "links": [
        {
          "url": "https://gitlab.com/gimbalabs/ppbl-2023/ppbl2023-plutus-template",
          "linkText": "PPBL 2023 Plutus Template"
        },
        {
          "url": "https://docs.cardano.org/native-tokens/learn",
          "linkText": "Cardano Docs: Learn About Native Tokens"
        }
      ],
      "videoHeading": "Minting Tokens with Native Scripts",
      "youtubeId": "P-vU1aLoj4c",
      "successComponent": false,
      "success": {
        "criteria": "",
        "text": ""
      },
      "assignmentComponent": true,
      "markdownComponent": false
    },
    {
      "slug": "2023",
      "introduction": [
        "Just like it is possible to mint tokens by building transactions with Cardano CLI, it is possible to build, sign, and submit a minting transaction directly with a Javascript application, by using Mesh SDK.",
        "This might be the first time you are seeing how to build a transaction in Mesh. If it is, welcome to the adventure!",
        "For now, just try to get this example working in your development environment - this is just the first of many transactions we'll build in PPBL 2023."
      ],
      "links": [
        {
          "url": "https://meshjs.dev/apis/transaction/minting",
          "linkText": "Official Mesh Documentation: Minting Transactions"
        },
        {
          "url": "https://meshjs.dev/guides/minting-on-nodejs",
          "linkText": "Mesh Guide: Minting on NodeJS"
        }
      ],
      "videoHeading": "Minting Tokens with Mesh",
      "youtubeId": "ovP9rnkFtZM",
      "successComponent": false,
      "success": {
        "criteria": "",
        "text": ""
      },
      "assignmentComponent": true,
      "markdownComponent": false
    },
    {
      "slug": "2024",
      "introduction": [
        "In this lesson, you will create a minting policy with Plutus. You'll notice that the process is similar to when we worked with native scripts in Lesson 202.2 - with a few new twists.",
        "First, you will see how to compile a minting validator written in PlutusTx, as first introduced in Lessons 101.3 and 101.4.",
        "Then, you will see how a minting transaction using a Plutus script compares to one using a native script.",
        "You might uncover some new questions about how Plutus works - let's continue to learn with examples."
      ],
      "links": [
        {
          "url": "https://gitlab.com/gimbalabs/ppbl-2023/ppbl2023-plutus-template",
          "linkText": "PPBL 2023 Plutus Template"
        }
      ],
      "videoHeading": "Compile a Plutus Minting Script from PlutusTx",
      "youtubeId": "iG4Th2dijD4",
      "successComponent": false,
      "success": {
        "criteria": "",
        "text": ""
      },
      "assignmentComponent": true,
      "markdownComponent": false
    },
    {
      "slug": "2025",
      "introduction": [
        "To continue building background knowledge, let's build a similar minting validator to the one we built in Lesson 202.4, but this time with Aiken!",
        "Which approach will you prefer?",
        "The Cardano ecosystem is expanding rapidly right now. By comparing different smart contract languages, you will be able to see what all of them have in common: this will help you build a mental model of how Plutus and Cardano work. You will also notice differences between languages. When you find something unique about a language that excites you, we hope that you'll follow your interests."
      ],
      "links": [
        {
          "url": "https://aiken-lang.org/",
          "linkText": "Aiken Smart Contract Platform"
        }
      ],
      "videoHeading": "Compile a Plutus Minting Script from Aiken",
      "youtubeId": "GQBRczbGjAg",
      "successComponent": false,
      "success": {
        "criteria": "",
        "text": ""
      },
      "assignmentComponent": true,
      "markdownComponent": false
    }
  ]
}
