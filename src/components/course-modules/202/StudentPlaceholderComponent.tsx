import { Flex, Box, Heading, Text } from "@chakra-ui/react";
import * as React from "react";
import { StatusBox } from "@/src/components/lms/Status/StatusBox";
import { useAddress, useAssets } from "@meshsdk/react";
import { NativeScript, resolveNativeScriptHash, resolvePaymentKeyHash } from "@meshsdk/core";
import { TX_FROM_ADDRESS_WITH_POLICYID } from "../102/queries";
import { useLazyQuery } from "@apollo/client";
import { contributorTokenPolicyId } from "@/src/cardano/plutus/contributorPlutusMintingScript";
import { PPBLContext } from "@/src/context/PPBLContext";

type Props = {
  children?: React.ReactNode;
};

const StudentPlaceholderComponent: React.FC<Props> = ({ children }) => {
  const walletAssets = useAssets();
  const address = useAddress(0);

  const [cliTokenPresent, setCliTokenPresent] = React.useState(false);
  const [cliTokenPolicyId, setCliTokenPolicyId] = React.useState<string | undefined>(undefined);

  const [meshTokenPresent, setMeshTokenPresent] = React.useState(false);
  const [meshTokenPolicyId, setMeshTokenPolicyId] = React.useState<string | undefined>(undefined);

  const [plutusTxTokenPresent, setPlutusTxTokenPresent] = React.useState(false);
  const plutusTxTokenPolicyId = "b4dff8a4bf58ef312cfc498231d4385349cdf9bc39e3bd0278f7637e";

  const [aikenTokenPresent, setAikenTokenPresent] = React.useState(false);
  const aikenTokenPolicyId = "9ff7cbc943842fff04e6b3b5a4abf9068bb73bccd2f314f5c34a9343";

  const ppblContext = React.useContext(PPBLContext);

  React.useEffect(() => {
    // Module 202.2: Native Script CLI
    if (ppblContext.cliAddress && ppblContext.cliAddress !== "" && walletAssets !== undefined) {
      const pubKeyHash = resolvePaymentKeyHash(ppblContext.cliAddress);
      const nativeScript: NativeScript = {
        type: "sig",
        keyHash: pubKeyHash,
      };
      const policyId = resolveNativeScriptHash(nativeScript);
      setCliTokenPolicyId(policyId);
      setCliTokenPresent(walletAssets?.some((a) => a.unit.substring(0, 56) == policyId));
    }

    // Module 202.3: Mesh
    if (address && walletAssets !== undefined) {
      const pubKeyHash = resolvePaymentKeyHash(address);
      const nativeScript: NativeScript = {
        type: "sig",
        keyHash: pubKeyHash,
      };
      const policyId = resolveNativeScriptHash(nativeScript);
      setMeshTokenPolicyId(policyId);
      setMeshTokenPresent(walletAssets?.some((a) => a.unit.substring(0, 56) == policyId));
      setPlutusTxTokenPresent(walletAssets?.some((a) => a.unit.substring(0, 56) == plutusTxTokenPolicyId));
      setAikenTokenPresent(walletAssets?.some((a) => a.unit.substring(0, 56) == aikenTokenPolicyId));
    }
  }, [walletAssets, address, ppblContext.cliAddress]);

  return (
    <>
      <Box border="1px" p="3" my="5">
        <Text>Expected Policy IDs</Text>
        <Text>
          {" "}
          {ppblContext.connectedContribToken ? (
            <Text>PPBL 2023 Token: {ppblContext.connectedContribToken}</Text>
          ) : (
            <Text>No PPBL 2023 Token found.</Text>
          )}
        </Text>
        <Box bg="theme.light" color="theme.dark" px="3" m="1">
          <Text>SLT 202.2</Text>
          <Text>
            What is the CLI Address associated with this Browser Wallet?{" "}
            {ppblContext.cliAddress ? ppblContext.cliAddress : "No CLI Address found"}
          </Text>

          {cliTokenPolicyId ? (
            <Text>
              If you mint a token using the Public Key Hash of the your CLI Wallet, it will have the Policy ID:{" "}
              {cliTokenPolicyId}
            </Text>
          ) : (
            <Text>Connect a Wallet to see expected Policy ID</Text>
          )}
        </Box>
        <Box bg="theme.light" color="theme.dark" px="3" m="1">
          <Text>SLT 202.3</Text>
          {meshTokenPolicyId ? (
            <Text>
              If you mint a token using the Public Key Hash of the connected Browser wallet, it will have the Policy ID:{" "}
              {meshTokenPolicyId}
            </Text>
          ) : (
            <Text>Connect a Wallet to see expected Policy ID</Text>
          )}
        </Box>
        <Box bg="theme.light" color="theme.dark" px="3" m="1">
          <Text>SLT 202.4</Text>
          <Text>Expected PlutusTx Policy ID: {plutusTxTokenPolicyId}</Text>
        </Box>
        <Box bg="theme.light" color="theme.dark" px="3" m="1">
          <Text>SLT 202.5</Text>
          <Text>Expected Aiken Policy ID: {aikenTokenPolicyId}</Text>
          <Text fontSize="sm" fontWeight="bold">Note: When a Cardano Smart Contract language is updated, it can result in the derivation of new a different PolicyID. If you do not get the same PolicyID as the one shown here, check the version of Aiken you have installed, and drop a line on Discord if you have questions.</Text>
          <Text fontSize="sm" fontWeight="bold">Updated 2023-09-04 with Aiken version <pre>1.0.16-alpha</pre></Text>
        </Box>
      </Box>
    </>
  );
};

export default StudentPlaceholderComponent;
